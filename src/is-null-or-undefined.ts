/**
 * @history:
 * ['31.10.18'] - Изменен принцип действия, пеперь {@function isNullOrUndefined} остановится и - <br>
 *  - вернет {bool} после первого же совпадения с условием
 * ['06.11.18'] - Добавленна проверка на строку 'null' в большей степени необходимая для работы со storage, так как там все значения string
 * */
export function isNullOrUndefined(...values: any[]): boolean {
    for (let i = 0, valuesLength = values.length; i < valuesLength; i++) {
        if (values[i] === undefined || values[i] === null || values[i] === 'null') {
            return true;
        }
    }

    return false;
}

/**
 * Проверяет values на остутствие значений.<br>
 * Значение считается пустым, если оно:
 * <ul>
 *     <li>undefined</li>
 *     <li>null</li>
 *     <li>его length равен 0</li>
 *     <li>кол-во его собственных свойств равно 0</li>
 *     <li>все его элементы/свойства пустые</li>
 * </ul>
 *
 * @history:
 * [10.01.2019] - добавлена рекурсивная проверка элементов/свойств если проверяемое значение является массивом/объектом.
 */
export function isNullOrUndefinedOrEmpty(...values: Array<Array<any> | Object | string>): boolean {
    for (let i = 0, valuesLength = values.length; i < valuesLength; i++) {
        const value = values[i] as any;

        if (value === undefined || value === null || value.length === 0 || isEmptyObject(value)) {
            return true;
        }

        let subvalues: any[] = [];
        // Если это объект, необходимо проверить его свойства.
        if (typeof value === 'object') {
            // @ts-ignore
            subvalues = Object.values(value);
        }
        // Если это массив, необходимо проверить его элементы.
        if (value instanceof Array) {
            subvalues = value;
        }
        // Если все свойства/элементы пустые, то объект/массив считается пустым.
        if (subvalues.length > 0 && subvalues.length === subvalues.filter(subvalue => isNullOrUndefinedOrEmpty(subvalue)).length) {
            return true;
        }
    }

    return false;
}

export function isNumber(obj: number): boolean {
    return typeof obj === 'number' && !isNaN(obj);
}

/**
 * Проверка обратная {@link isNullOrUndefinedOrEmpty}.
 */
export function notEmpty(...values: any[]): boolean {
    return !isNullOrUndefinedOrEmpty(...values);
}

export function allIsNullOrUndefined(...values: any[]): boolean {
    for (let i = 0; i < values.length; i++) {
        if (values[i] !== undefined && values[i] !== null) {
            return false;
        }
    }

    return true;
}

function isEmptyObject(value: any): boolean {
    return typeof value === 'object' && Object.keys(value).length === 0;
}
