import { isNullOrUndefined } from './is-null-or-undefined';
import { objectCopy } from './object-copy';

/**
 * Функция objectComparer рекурсивно сравнивает objA && objB
 * и возвращает точно такойже обьект но только с полями которые изменились
 * */
export function objectComparer<T>(updatedObj: T, defaultObject: T): T {
  const _updatedObj = objectCopy(updatedObj);
  const _defaultObject = objectCopy(defaultObject);

  const notFilteredComparerObject = objectCopy(comparerValueGenerator(_updatedObj, _defaultObject)) || {};

  /** Тут происходит фильтрация полей так как может быть кейс:
   *  { key1: someValue, key2: undefined }
   *  Верётся обьект только с заполненными полями или с null
   *  */
  const comparerObject = Object.create(null) as T;

  Object.keys(notFilteredComparerObject).forEach(key => {
    const objectValue = notFilteredComparerObject[key];

    /** Если объект не содержит в себе изменений */
    if (objectValue instanceof Object && Object.keys(objectValue).length === 0) { return; }

    /** Если value === undefined */
    if (objectValue === undefined) { return; }

    comparerObject[key] = objectValue;
  });

  return comparerObject;
}

function comparerValueGenerator(updatedObj, defaultObject): Object {
  let comparerValue: any = Object.create(null);

  for (const changeKey in updatedObj) {
    if (updatedObj.hasOwnProperty(changeKey)) {

      /**
       * Если поле === Object и существует
       * */
      if (updatedObj[changeKey] instanceof Object && !isNullOrUndefined(defaultObject)) {
        comparerValue[changeKey] = comparerValueGenerator(updatedObj[changeKey], defaultObject[changeKey]);
      }

      /**
       * Если поле массив то проще сравнивать его в виде строк
       * */
      if (updatedObj[changeKey] instanceof Array) {
        const newValue = JSON.stringify(updatedObj[changeKey]);
        const defaultValue = JSON.stringify(defaultObject[changeKey]);

        if (newValue !== defaultValue) {
          comparerValue[changeKey] = updatedObj[changeKey];
        }
      }

      /**
       * Тут подразумеваесть что поле имеет синтаксис key: value и не нуждается в проверках
       * */
      if (!(updatedObj[changeKey] instanceof Array) && !(updatedObj[changeKey] instanceof Object && !isNullOrUndefined(defaultObject))) {
        if (isNullOrUndefined(defaultObject)) {
          comparerValue = updatedObj;

        } else {
          if (defaultObject[changeKey] !== updatedObj[changeKey]) {
            comparerValue[changeKey] = updatedObj[changeKey];
          }
        }
      }
    }
  }

  return (Object.keys(comparerValue).length > 0) ? comparerValue : {};
}
