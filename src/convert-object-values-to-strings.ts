/**
 * Конвертирует значения всех полей объекта в строки или в массив строк.
 * Игнорирует значения типа Object, undefined и массивы массивов. Null, NaN (и другие аналогичные значения типа Number) записывает как
 * null', 'NaN' и т.д.
 *
 * @example
 * convertObjectValuesToStrings({
 *     numberFiled: 123,
 *     stringFiled: 'qwerty',
 *     arrayFieldOne: [1, 234, 56],
 *     arrayFieldTwo: [
 *         { id: 2354645, value: 'qwewfg5' },
 *         { id: 6578456, value: 'gh678yj' },
 *     ],
 *     objectFiled: { foo: 'Foo' },
 *     nullField: null,
 *     NaNField: NaN,
 *     undefinedField: undefined,
 *     arrayOfArraysField: [
 *         [0, 12],
 *         [2, 9],
 *         [5, 1],
 *     ],
 * });
 * // На выходе будет объект:
 * // {
 * //     numberFiled: '123',
 * //     stringFiled: 'qwerty',
 * //     arrayFieldOne: ['1', '234', '56'],
 * //     nullField: 'null',
 * //     NaNField: 'NaN',
 * // }
 *
 * @param sourceObject - конвертируемый объект
 */
export function convertObjectValuesToStrings<T>(sourceObject: T): Record<keyof T, string | string[]> {
  const convertedObject = Object.create(null) as Record<keyof T, string | string[]>;
  const sourceObjectEntries = Object.entries(sourceObject);

  for (let i = 0, len = sourceObjectEntries.length; i < len; i++) {
    const [key, value] = sourceObjectEntries[i];
    const convertedValue = _tryConvertValueToString(value, true);
    if (convertedValue !== undefined) {
      convertedObject[key] = convertedValue;
    }
  }

  return convertedObject;
}

function _tryConvertValueToString(value: any, doConvertArrays: false): string | undefined;
function _tryConvertValueToString(value: any, doConvertArrays: true): string | string[] | undefined;

function _tryConvertValueToString(value: any, doConvertArrays: boolean): string | string[] | undefined {
  switch (true) {
    case typeof value === 'string':
      return value;
    case typeof value === 'number':
      return value.toString();
    case doConvertArrays && value instanceof Array:
      return _tryConvertArrayValuesToString(value);
    case value === null:
      return 'null';
    case typeof value === 'object':
    case value === undefined:
      return undefined;
  }
}

function _tryConvertArrayValuesToString(sourceArray: Array<any>): string[] | undefined {
  const convertedArray: string[] = [];

  for (let i = 0, len = sourceArray.length; i < len; i++) {
    const convertedArrayItem = _tryConvertValueToString(sourceArray[i], false);
    if (convertedArrayItem !== undefined) {
      convertedArray.push(convertedArrayItem);
    }
  }

  return convertedArray.length > 0 ? convertedArray : undefined;
}
