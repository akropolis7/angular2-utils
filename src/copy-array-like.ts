export function copyArrayLike(arrayLike: {[index: number]: any}): any[] {
  return [].slice.call(arrayLike);
}
