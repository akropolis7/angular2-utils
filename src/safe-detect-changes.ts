/**
 * Обертка для безопасного вызова detectChanges.
 */
// @ts-ignore
import { ChangeDetectorRef } from '@angular/core';

export function safeDetectChanges(changeDetectorRef: ChangeDetectorRef): void {
  setTimeout(() => {
    if (!changeDetectorRef['destroyed']) {
      changeDetectorRef.detectChanges();
    }
  });
}
