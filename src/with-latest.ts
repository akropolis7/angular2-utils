import { Observable, combineLatest } from 'rxjs';

/**
 * One-time receives the last values from all {@link observables} and calls a {@link callback} with them.
 */
export function withLatest(observables: Observable<any>[], callback: (...values: any[]) => void): void {
  if (typeof callback !== 'function') {
    return;
  }
  combineLatest(...observables).subscribe((values: any[]) => {
    callback(...values);
  }).unsubscribe();
}
