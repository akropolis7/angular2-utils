import { objectCopy } from './object-copy';

export function filterObject<T extends Object>(obj: T, callbackFn: (value: any) => boolean): T {
  const copy = objectCopy(obj);

  for (const property in copy) {
    if (copy.hasOwnProperty(property) && !callbackFn(copy[property])) {
      delete copy[property];
    }
  }

  return copy;
}
