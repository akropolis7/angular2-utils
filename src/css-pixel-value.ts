import { isNullOrUndefined } from './is-null-or-undefined';

export function cssPixelValue(value: any): string {
  if (isNullOrUndefined(value)) {
    return '';
  }
  return typeof value === 'string' ? value : `${value}px`;
}
