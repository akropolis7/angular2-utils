export function parseBoolean(value: any): boolean {
  switch (typeof value) {
    case 'string':
      return value === 'true';
    default:
      return !!value;
  }
}
