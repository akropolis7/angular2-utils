/**
 * Объеденяет объекты в один новый объект, который содержит в себе все их собственные свойства.<br>
 * Свойства с одинаковым именем переопределяются в том же порядке в каком объекты переданы в функцию.
 *
 * @example
 * const original: User = { name: 'Juliana', age: 19 };
 * const alter: User = fuse(original, { age: 25 }, { isHot: true });
 * // Объект 'alter' будет равен { name: 'Juliana', age: 25, isHot: true }, кроме того, это будет новый объект (новый экземпляр).
 */
export function fuse<T>(...pieces: Partial<T>[]): T {
  // @ts-ignore
  return Object.assign({}, ...pieces);
}
