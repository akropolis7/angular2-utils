import { isNullOrUndefined } from './is-null-or-undefined';

export function objectCopy<T>(object: T): T {
  if (isNullOrUndefined(object)) {
    return object;
  }
  return JSON.parse(JSON.stringify(object));
}
