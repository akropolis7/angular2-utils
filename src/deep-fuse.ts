import { isNullOrUndefined } from './is-null-or-undefined';

/**
 * Даная функция {@property @link deepFuse()} проводит проверку:
 * {@property updatedObj} - в дальнейшем ObjA;
 * {@property defaultObject} - в дальнейшем ObjB;
 *
 * Если в {A} есть поле {n} и в {B} есть точно такое-же поле {n},
 * производит замену:
 * в поле {B{n}} записать значение из поля {A{n}}
 *
 * @history
 * 23.10.18
 * Добалена проверка на Array для исправления перезаписи масивов
 * */

export function deepFuse(updatedObj: object, defaultObject: object): any {
  for (const changeKey in updatedObj) {
    if (updatedObj.hasOwnProperty(changeKey)) {
      /**
       * Если поле === Object и существует
       * */
      if (updatedObj[changeKey] instanceof Object && !isNullOrUndefined(defaultObject)) {
        defaultObject[changeKey] = deepFuse(updatedObj[changeKey], defaultObject[changeKey]);
      }

      /**
       * Если поле массив то проще сравнивать его в виде строк
       * */
      if (updatedObj[changeKey] instanceof Array && !isNullOrUndefined(defaultObject)) {
        const newValue = JSON.stringify(updatedObj[changeKey]);
        const defaultValue = JSON.stringify(defaultObject[changeKey]);

        if (newValue !== defaultValue) {
          defaultObject[changeKey] = updatedObj[changeKey];
        }
      }
      /**
       * Тут подразумеваесть что поле имеет синтаксис key: value и не нуждается в проверках
       * */
      if (!(updatedObj[changeKey] instanceof Array) && !(updatedObj[changeKey] instanceof Object && !isNullOrUndefined(defaultObject))) {
        if (isNullOrUndefined(defaultObject)) {
          defaultObject = updatedObj;

        } else {
          if (defaultObject[changeKey] !== updatedObj[changeKey]) {
            defaultObject[changeKey] = updatedObj[changeKey];
          }
        }
      }

    }
  }

  return defaultObject;
}
