/**
 * Замораживает (дает иммутабельным) переданный объект.<br>
 * Возвращаеат замороженный объект, и это тот же экземляр, что был передан в функцию.
 */
export function deepFreeze<T>(obj: T, ignore: Array<keyof T> = []): T {
  if (obj instanceof HTMLElement) {
    return obj;
  }
  for (const prop in obj) {
    if (obj.hasOwnProperty(prop) && typeof obj[prop] === 'object' &&
      !ignore.indexOf(prop)) {

      obj[prop] = deepFreeze(obj[prop]);
    }
  }
  return Object.freeze(obj);
}
