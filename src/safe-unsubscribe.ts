import { Subscription } from 'rxjs';

export function safeUnsubscribe(...subscriptions: Subscription[]): void {
  subscriptions.forEach((subs: Subscription) => {
    if (subs && !subs.closed) {
      subs.unsubscribe();
    }
  });
}
