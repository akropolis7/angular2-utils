import { isNullOrUndefined } from './is-null-or-undefined';

/**
 * Интерполирует входящую строку с переданным параметрами.<br>
 * Возвращает массив примитивов (строк, чисел) и объектов, полученный в ходе разбиения входящей строки на части и их замены на переданные
 * параметры.
 *
 * @example
 * const str = 'Когда-то и меня вела дорога приключений...А потом мне прострелили {{target}}...';
 * const interpolatedStr = _interpolateParams(str, { target: 'колено' });
 * // interpolatedStr == ['Когда-то и меня вела дорога приключений...А потом мне прострелили', 'колено', '...'];
 *
 * @param message - строка, содержащая в себе параметры, экранированные двойными фигурными скобками
 * @param interpolationParams - параметры интерполяции
 */
export function interpolate(message: string, interpolationParams: Object): any[] {
  if (isNullOrUndefined(interpolationParams)) {
    return [message];
  }
  return message
    .match(/({{[^}]*}})|([^{]+)/g)
    .map((arg: string) => {
        // @ts-ignore
      if (arg.includes('{{')) {
        const param = arg.slice(2, -2);
        return interpolationParams[param];
      }
      return arg.trim();
    });
}
