export function generateMockData<T>(generator: (i: number) => T, count: number): T[] {
  const items: T[] = [];
  for (let i = 0, len = count; i < len; i++) {
    const item = generator(i);
    items.push(item);
  }
  return items;
}
