/**
 * Простейшая, как первые одноклеточные, функция для явной типизации везде где можно и нельзя. Типизруй, оптимизируй и еби гусей!
 *
 * @example
 * const MY_CONST = {
 *     foo: '123123123',
 *     bar: as<MyInterface>({
 *         lol: true,
 *         kek: true,
 *         cheburek: true,
 *     }),
 * };
 */
export function as<T>(value: T): T {
  return value;
}
